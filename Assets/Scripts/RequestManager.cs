using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AAManager;

public enum RequestType { PLUMBING, ELECTRIC, CLEANING, OTHERS}
[System.Serializable]
public class RequestInfo {
    public string uniqueID;
    public string title;
    public string description;
    public RequestType requestType;
    //public 
}

public class RequestManager : Singleton<RequestManager>
{
  
}
