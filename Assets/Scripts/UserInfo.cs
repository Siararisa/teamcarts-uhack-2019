﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UserInfo 
{
    public string firstName;
    public string lastName;
    public Sprite profile;
    public string occupation;
    public float averageRating;
    public int servicesAsked;
    public int servicesHelped;
}
